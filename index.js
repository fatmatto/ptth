'use strict'

const http = require('follow-redirects').http
const https = require('follow-redirects').https
const url = require('url')
const qs = require('querystring')

const interceptors = {
  request: {
    use: (interceptor) => {
      if (typeof interceptor !== 'function') {
        throw new Error('interceptor must be a function')
      }
      ptth.interceptors.request.interceptors.push(interceptor)
    },
    interceptors: []
  },
  response: {
    use: (interceptor) => {
      if (typeof interceptor !== 'function') {
        throw new Error('interceptor must be a function')
      }
      ptth.interceptors.response.interceptors.push(interceptor)
    },
    interceptors: []
  }
}

/**
 * Performs an HTTP(s) request
 * @param {Object} config
 * @param {String} config.method Request method
 * @param {String} [config.url] Request uri
 * @param {String} [config.uri] Request uri
 * @param {Object} [config.headers] Request Headers object
 * @param {Object} [config.query] Request query object
 * @param {Object} [config.body] Request body object
 * @param {Object} [config.pipeTo] A writeable stream to attach to the request's readable stream
 * @param {Boolean} [config.json] If set to false the body won't be parsed as json (with JSON.parse). Defaults to true
 *
 * @returns {Promise}
 * @throws {Error}
 */
function ptth (conf) {
  const config = Object.assign({}, conf)
  if (config.uri) {
    process.emitWarning('Warning, the "uri" parameter is going to be deprecated in favor of "url"')
  }
  if (config.url && config.uri) {
    throw new Error('Cannot specify both url and uri')
  }
  if (config.url && !config.uri) {
    config.uri = config.url
    delete config.url
  }
  let uri = new url.URL(config.uri)

  let requestOptions = {
    protocol: uri.protocol,
    path: uri.pathname,
    host: uri.hostname,
    port: uri.port,
    headers: config.headers || {},
    method: config.method
  }
  const httpOptions = ['family', 'socketPath', 'auth', 'agent', 'createConnection', 'timeout', 'setHost', 'protocol']

  httpOptions.forEach(opt => {
    if (config.hasOwnProperty(opt)) {
      requestOptions[opt] = config[opt]
    }
  })

  let queryInUri = {}
  if (uri.search) {
    let search = uri.search
    if (search[0] === '?') {
      search = search.slice(1)
    }
    queryInUri = qs.parse(search)
  }

  const userQuery = config.query || {}
  const finalQueryObject = Object.assign({}, queryInUri, userQuery)

  if (Object.keys(finalQueryObject).length) {
    if (requestOptions.path.indexOf('?') < 0) {
      requestOptions.path = requestOptions.path + '?'
    }
    requestOptions.path += qs.stringify(finalQueryObject)
  }

  let promise = new Promise((resolve, reject) => {
    let client = null
    if (requestOptions.protocol.indexOf('https') > -1) {
      client = https
    } else {
      client = http
    }
    // Adding the correct content-type for json payloads
    if (typeof config.body === 'object') {
      config.body = JSON.stringify(config.body)
      requestOptions.headers['Content-Type'] = 'application/json'
    }

    if (interceptors.request.interceptors.length) {
      interceptors.request.interceptors.forEach(interceptor => {
        requestOptions = interceptor(requestOptions)
      })
    }
    const req = client.request(requestOptions, (res) => {
      // Handling streams
      if (config.pipeTo) {
        return res.pipe(config.pipeTo)
      }

      res.resume()
      let data = ''
      res.on('data', (chunk) => { data += chunk })
      res.on('error', reject)
      res.on('end', async () => {
        res.body = data
        // Unless config.json is explicitly set to false
        // we try to parse the body as json string
        // if we succed, we return the object
        // if we fail and the json parameter was set to true, we reject with an error
        // if we fail and the json parameter was NOT set to true, we just return the raw body
        if (config.json !== false) {
          try {
            res.body = JSON.parse(res.body)
          } catch (e) {
            if (config.json === true) {
              reject(new Error('config.json is set to true, but the response is not valid JSON.'))
            } else {
              res.body = data
            }
          }
        }

        if (interceptors.response.interceptors.length) {
          let o = null
          for (const fn of interceptors.response.interceptors) {
            o = await fn(res)
          }
          resolve(o)
        }
        resolve(res)
      })
    })

    req.on('error', reject)

    // If we have body, we have to write it
    if (config.body) {
      req.write(config.body)
    }

    req.end()
  })

  return promise
}

ptth.interceptors = interceptors

module.exports = ptth
