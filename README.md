# PTTH

<p align="center"> Small, promise-based  wrapper around node native http module </p>

<hr/>
Hello!

<p> I wanted a simple promise layer on top of native http/https nodejs modules</p>

### List of features

* Promises
* Simplicity



### GET request example

```javascript


const http = require('ptth')

http({
  method: 'GET',
  uri: 'http://example.org',
  query: {
    boolean: true,
    number:1,
    string: "Ciao"
  },
  headers: {
    'x-request-id': '121121212'
  }
})
  .then((resp) => {
    console.log(`Status code is ${resp.statusCode}`)
    console.log('Headers:', resp.headers)
    console.log('Body:', resp.body)
  })
  .catch(console.error)
```

### POST request example
```javascript
  http({
  method: 'POST',
  uri: 'http://example.org',
  body: {
    boolean: true,
    number:1,
    string: "Ciao"
  },
  headers: {
    'x-request-id': '121121212'
  }
})
  .then((resp) => {
    console.log(`Status code is ${resp.statusCode}`)
    console.log('Headers:', resp.headers)
    console.log('Body:', resp.body)
  })
  .catch(console.error)
```
### Using Async/Await
```javascript

  let response = await http({
  method: 'POST',
  uri: 'http://example.org',
  body: {
    boolean: true,
    number:1,
    string: "Ciao"
  },
  headers: {
    'x-request-id': '121121212'
  }
})

console.log(`Status code is ${response.statusCode}`)
console.log('Headers:', response.headers)
console.log('Body:', response.body)
```

### Using a custom HTTP Agent
```javascript

const https = require('https')

  let response = await http({
    agent: new https.Agent({
      rejectUnauthorized: false
    }),
    method: 'GET',
    uri: 'https://somesitewithabadSSLcertificate.com'
  })
}

console.log(`Status code is ${response.statusCode}`)
console.log('Headers:', response.headers)
console.log('Body:', response.body)
```


### Params

| Name | Type | Description | Required | Default |
| ---- | ---- | ----------- | -------  | ------- |
| method  | String | HTTP Method | Yes    |    -    |
| uri     | String | HTTP Uri   | Yes    |    -    |
| headers | Object | HTTP headers | No    |    -    |
| query   | Object | HTTP query object that builds the querystring | No      |    -    |
| body    | Object | HTTP body | No       |    -    |
| pipeTo  | Stream | Writable stream | No     |    -    |
| json    |Boolean | If set to false the response is not parsed as json | No   |     true    |


### Interceptors

**Request interceptors**

```javascript
  http.interceptors.request.use(config => {
    config.headers.foo = 'bar'
    return config
  })
```

**Response interceptors**

```javascript
  http.interceptors.response.use(response => {
    // Do something with response
    return response
  })
```

### Download & Installation

```shell
$ npm i @fatmatto/ptth
```
### Contributing
Keep it simple. Keep it minimal. Don't put every single feature just because you can.

### Authors or Acknowledgments

* Mattia 'fat' Alfieri

### License

This project is licensed under the MIT License
