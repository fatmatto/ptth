# 📦 1.6.0 (4 Feb 2024)
- [faa04](https://gitlab.com/fatmatto/ptth/commit/faa04fbaca6a1c9f94f607ca2677d7243b784e6f)  1.6.0
# 📦 1.5.7 (10 May 2021)
- [2a5e4](https://gitlab.com/fatmatto/ptth/commit/2a5e42b1ec007e8a596980dc50182724b4d0114c)  Release 1.5.7
- [e6aeb](https://gitlab.com/fatmatto/ptth/commit/e6aeb1733d7f47f82b4532db462d97d74190ca66)  1.5.7
- [2c568](https://gitlab.com/fatmatto/ptth/commit/2c568325563f2cf98d1dda418a123d6c585fdcb7)  chore: switch changelog generator
- [1f958](https://gitlab.com/fatmatto/ptth/commit/1f9588e5660c7ff7afc79f6bdb100af7b1f1e8e3)  fix: include types in package.json
# 📦 1.5.6 (10 May 2021)
- [b3486](https://gitlab.com/fatmatto/ptth/commit/b3486253fb4515bece8a5b0acb41e1db87ed7e4b)  1.5.6
- [754fd](https://gitlab.com/fatmatto/ptth/commit/754fd5b595474c258d50f448d22f7ec2044b6c2d)  feat: add typescript typings; chore: update dependencies
# 📦 1.5.5 (8 Dec 2020)
- [21232](https://gitlab.com/fatmatto/ptth/commit/2123236b3b1d9cc4cdf5ccbb87ee48f8a0490351)  1.5.5
- [f3899](https://gitlab.com/fatmatto/ptth/commit/f38991da9d03eb0cb6b51c9e60fc7347f29f2901)  fix: merge querystring from config and url
# 📦 1.5.4 (20 Nov 2020)
- [3af99](https://gitlab.com/fatmatto/ptth/commit/3af99d49196ce2099f7ba0141f3e302c6912228b)  Release 1.5.4
- [f03e4](https://gitlab.com/fatmatto/ptth/commit/f03e4e876fa57f29c601c99c0c4c47d4d274b3d9)  1.5.4
- [13ad6](https://gitlab.com/fatmatto/ptth/commit/13ad60e4c2928e343e94c825ed587fac38d0f38a)  fix: get-port should be a dev dependency
# 📦 1.5.3 (20 Nov 2020)
- [9e591](https://gitlab.com/fatmatto/ptth/commit/9e59152d9c142286defc5514b20fecc01263afdb)  1.5.3
- [9f43d](https://gitlab.com/fatmatto/ptth/commit/9f43d556a61b47f54c998e6a88cb3f18011fb467)  Switch to CommonJS require
- [1c69f](https://gitlab.com/fatmatto/ptth/commit/1c69f1f81a318d596d79ea1befaf838dd98445bd)  Update dependencies
- [74cf3](https://gitlab.com/fatmatto/ptth/commit/74cf38c5276cafb0128a8a95964a18c73a453b50)  fix: copy the request config object to avoid side effects
# 📦 1.5.2 (6 Aug 2019)
- [9a4bd](https://gitlab.com/fatmatto/ptth/commit/9a4bd8fc0634d0cf21106ec5be15b813fecf3826)  Release 1.5.2
- [0586b](https://gitlab.com/fatmatto/ptth/commit/0586b9c0623755ede05182228850e68f0ec76bfa)  1.5.2
- [73b10](https://gitlab.com/fatmatto/ptth/commit/73b10d3afe458c20333f50fad775f575af67cf9b)  Fix JSDoc annotation
# 📦 1.5.1 (5 Aug 2019)
- [4c3cd](https://gitlab.com/fatmatto/ptth/commit/4c3cd6d74e533be422e6f0e4c44830e7dc633a1f)  Release 1.5.1
- [871c1](https://gitlab.com/fatmatto/ptth/commit/871c146a94111eccf0565d71a0803610cecfdabf)  1.5.1
- [af9db](https://gitlab.com/fatmatto/ptth/commit/af9db367959d0b9e66c2f5b0182e9946768f588a)  Fix release script to add package-lock
- [58b03](https://gitlab.com/fatmatto/ptth/commit/58b03124b09a4e9980c7045f6cdf99f5f257f1aa)  Fix dev dependencies
- [985b6](https://gitlab.com/fatmatto/ptth/commit/985b613ba4a688d9803a8341cde8d4181c7c1313)  Merge branch 'follow-redirects' into 'master'
- [4adf8](https://gitlab.com/fatmatto/ptth/commit/4adf8485812034dc7e4473ebaa206d7768b1cb08)  Follow redirects
- [1413e](https://gitlab.com/fatmatto/ptth/commit/1413ed1d4e02da9027b482c228150abcbaf4d554)  improved docs
# 📦 1.5.0 (25 Jul 2019)
- [440b1](https://gitlab.com/fatmatto/ptth/commit/440b10354717b8c181139411f57d551d9a46db33)  Release 1.5.0
- [f7595](https://gitlab.com/fatmatto/ptth/commit/f7595cae3d1af0247e5fdc4af9ba65b0a4901578)  1.5.0
- [a504b](https://gitlab.com/fatmatto/ptth/commit/a504b6079f49785e261feee0c313af0f33618179)  Ignore files in npm
- [78308](https://gitlab.com/fatmatto/ptth/commit/78308c89f844aacb44a4a19df3952b74f7439287)  Add pipeTo param
- [6829c](https://gitlab.com/fatmatto/ptth/commit/6829c1770c3744db2a0cfccb5636e6791a7601c5)  added lint stage to gitlab-ci
- [e978d](https://gitlab.com/fatmatto/ptth/commit/e978d7857371faba2ab091c35fbcda321d11e115)  Merge branch 'master' of gitlab.com:fatmatto/ptth
- [da1de](https://gitlab.com/fatmatto/ptth/commit/da1ded123352e94a5ec6a365e4fd2160bc803a16)  Add LICENSE
- [b7b3b](https://gitlab.com/fatmatto/ptth/commit/b7b3b3a9130e15e1ab5b979f842cfd7628a1fa5b)  style: 💄 Fixed lint with ESLint
- [8719a](https://gitlab.com/fatmatto/ptth/commit/8719ad050a875808c7c2bf9cf82c79cc59fe6a9c)  Added eslint [skip ci]
- [5cac6](https://gitlab.com/fatmatto/ptth/commit/5cac690233ab6208e9fa86cef9f0959ec4dde111)  added gitlab-ci file
- [f53dd](https://gitlab.com/fatmatto/ptth/commit/f53dddd14995d89ae3095a2580d43202cee39af5)  added --check-coverage to test command
- [a6695](https://gitlab.com/fatmatto/ptth/commit/a66959b16161d2b4241dd0e7182ad7561ae64878)  added npm publish to release script
- [b5a3f](https://gitlab.com/fatmatto/ptth/commit/b5a3f317978421ed7a64678236f941de1651d58c)  Merge branch 'master' of gitlab.com:fatmatto/ptth
- [dd2aa](https://gitlab.com/fatmatto/ptth/commit/dd2aa33f2b2b34f3ceaac121eaaab64ee941ab54)  Using npx to run tests
- [3c560](https://gitlab.com/fatmatto/ptth/commit/3c560e19bd15e8926f7978d96311a0a24049110f)  Update package.json
- [421eb](https://gitlab.com/fatmatto/ptth/commit/421ebd6197eeba6ff74681b3af0d0b13c96854b8)  Update package.json
# 📦 1.4.3 (22 Jan 2019)
- [6cb2e](https://gitlab.com/fatmatto/ptth/commit/6cb2e2be6fc585f961d1d9c8d34a159e89a1b1ca)  Release 1.4.3
- [4626c](https://gitlab.com/fatmatto/ptth/commit/4626c2c1d5165516d3f602cfcf9b6d287986684d)  1.4.3
- [8951f](https://gitlab.com/fatmatto/ptth/commit/8951f844d58c2163a58b68ac89de9ebd4843a44c)  chore: 🤖 Release script now parses the release type
- [cdd84](https://gitlab.com/fatmatto/ptth/commit/cdd84ec9a1acd240029fbe5e95a90d4dfdb59d58)  Rejecting errors emitted by the request object
# 📦 1.4.2 (21 Jan 2019)
- [9b0be](https://gitlab.com/fatmatto/ptth/commit/9b0be3b206f38a3e427bc6289db664055933fab1)  Release 1.4.2
- [ee68f](https://gitlab.com/fatmatto/ptth/commit/ee68fb9d81daa47f294597cc45b74b132bc12879)  1.4.2
- [6e294](https://gitlab.com/fatmatto/ptth/commit/6e294136a63e43e3e91dd46c574da77bbe702376)  Added tests
# 📦 1.4.1 (21 Jan 2019)
- [35b14](https://gitlab.com/fatmatto/ptth/commit/35b1499541ada824214b23736806577456b89d83)  Release 1.4.1
- [c1a0f](https://gitlab.com/fatmatto/ptth/commit/c1a0fc673d9d9dcf14afcf7df0c693510a453cc8)  1.4.1
- [6ab01](https://gitlab.com/fatmatto/ptth/commit/6ab0160413edece3bf62712eccc54aab851ceb3d)  📖  Updated README with moar examples
- [68c65](https://gitlab.com/fatmatto/ptth/commit/68c657c581de10d4274d62cf6a5b608c28397df9)  Fixed bug which prevented the library from sending the Content-Type header along with the request
- [8e096](https://gitlab.com/fatmatto/ptth/commit/8e09644e0c33eff230d8f3d11de9ae0d50814a59)  updated package.json
# 📦 1.4.0 (16 Jan 2019)
- [36b7a](https://gitlab.com/fatmatto/ptth/commit/36b7adabb65c1feed6c5df6c311bf0a46cfaa0b5)  Release 1.4.0
- [fafcd](https://gitlab.com/fatmatto/ptth/commit/fafcd441e3778e6c221a4de0b066f9f0a252d98f)  1.4.0
- [9329d](https://gitlab.com/fatmatto/ptth/commit/9329d0b27434943a1b312b705e21cb39402a2b32)  https support
# 📦 1.3.0 (16 Jan 2019)
- [5a1ff](https://gitlab.com/fatmatto/ptth/commit/5a1ff8c91b00b20882f9f3b2f2ef9293e68a615e)  Release 1.3.0
- [fc3e1](https://gitlab.com/fatmatto/ptth/commit/fc3e1ac4f6d1e9df34dc44c8e7b6d854d32dabc3)  1.3.0
- [9f953](https://gitlab.com/fatmatto/ptth/commit/9f95377ed614a9ea5de6e78e49fb5d9219463a12)  Optimistically trying to parse json unless stated otherwise via the "json" Bool param
# 📦 1.2.0 (16 Jan 2019)
- [8973c](https://gitlab.com/fatmatto/ptth/commit/8973c37211ab2406e6747960e20db43c5096364d)  Release 1.2.0
- [1828f](https://gitlab.com/fatmatto/ptth/commit/1828f4776b40d8a3257da5c2eb36266ec72e8009)  1.2.0
- [fbe73](https://gitlab.com/fatmatto/ptth/commit/fbe73b0c8cdb3b8330862b6548ca4b1c75752bd7)  chore: 🤖 Added publish to release scripts
- [98ecb](https://gitlab.com/fatmatto/ptth/commit/98ecb678a7c6fe81cf73fa285fc94aa5956f919f)  Parsing JSON when response shows application/json content-type
- [50df8](https://gitlab.com/fatmatto/ptth/commit/50df802f55dfaa164f5f552dbbcf0f6a15cfae1c)  Updated readme
# 📦 1.1.0 (16 Jan 2019)
- [77568](https://gitlab.com/fatmatto/ptth/commit/775687cb079c36282cee25f25b417bed46dddc4d)  Release 1.1.0
- [036f5](https://gitlab.com/fatmatto/ptth/commit/036f5f02bb7af62bd74e3cfce3d7c168d09988c9)  1.1.0
- [afae4](https://gitlab.com/fatmatto/ptth/commit/afae4eb388915ac0e1c601a32abb69b58a771ff3)  Now actually resolving the response. Added readme
- [cffbd](https://gitlab.com/fatmatto/ptth/commit/cffbdc1fb45d64598fe5a01d9788dcf2bc460136)  Added release script
- [daf5b](https://gitlab.com/fatmatto/ptth/commit/daf5b5c8c8719521a03b3f20d8b60732745235d8)  First commit
