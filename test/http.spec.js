const test = require('ava')
const http = require('../index')
const { createServer } = require('./helpers/server')

let s = null

test.before('setup', async () => {
  s = await createServer()

  s.on('/', (request, response) => {
    response.end('ok')
  })

  s.on('/send-me-json', (request, response) => {
    response.end('no json ahahahahha')
  })

  s.on('/merge-querystring?super=mario&donkey=kong', (request, response) => {
    response.end('okeydokey')
  })

  s.on('/post', (request, response) => {
    response.setHeader('method', request.method)
    request.pipe(response)
  })

  s.on('/304', (request, response) => {
    response.statusCode = 304
    response.end()
  })

  s.on('/404', (request, response) => {
    response.statusCode = 404
    response.end('not')
  })

  s.on('/?foo=bar', (request, response) => {
    response.end('baz')
  })

  s.on('/request-interceptor', (request, response) => {
    response.end(request.headers.secret)
  })

  s.on('/response-interceptor', (request, response) => {
    response.end('response-interceptor')
  })

  await s.listen(s.port)
})

test.after('cleanup', async () => {
  await s.close()
})

test('simple get request', async t => {
  const response = await http({
    method: 'GET',
    url: s.url
  })
  t.is(response.body, 'ok')
})
test('simple get request matches params', async t => {
  const response = await http({
    method: 'GET',
    url: s.url,
    query: {
      foo: 'bar'
    }
  })
  t.is(response.body, 'baz')
})

test('simple get request matches params 2', async t => {
  const response = await http({
    method: 'GET',
    url: `${s.url}?foo=bar`
  })
  t.is(response.body, 'baz')
})

test('Merge querystring params from url and query object', async t => {
  const response = await http({
    method: 'GET',
    url: `${s.url}/merge-querystring?super=mario`,
    query: {
      donkey: 'kong'
    }
  })
  t.is(response.body, 'okeydokey')
})

test('simple post request', async t => {
  const rand = Math.random().toString(36).substring(7)
  const response = await http({
    method: 'POST',
    url: s.url + '/post',
    body: {
      value: rand
    }
  })
  t.is(response.body.value, rand)
})

test('Should throw for a non valid json response', async t => {
  // try { await fn(); } catch (err) { /* ... */ }
  try {
    await http({
      method: 'GET',
      url: s.url + '/send-me-json',
      json: true
    })
  } catch (err) {
    t.is(err.message, 'config.json is set to true, but the response is not valid JSON.')
  }
})

test('Should throw for passing both uri and url', async t => {
  t.throws(() => {
    http({
      method: 'GET',
      uri: s.url + '/send-me-json',
      json: true,
      url: s.url + '/send-me-json'
    })
  })
})

test('Should run request interceptor', async t => {
  http.interceptors.request.use(config => {
    config.headers.foo = 'bar'
    return config
  })

  const secret = Math.random()
  const response = await http({
    method: 'GET',
    headers: {
      secret
    },
    url: s.url + '/request-interceptor'
  })
  t.is(response.body, secret)
})

test('Should run response interceptor', async t => {
  const secret = Math.random()
  http.interceptors.response.use(async response => {
    if (response.body === 'response-interceptor') {
      response.body = { value: secret }
    }
    return response
  })

  const response = await http({
    method: 'GET',
    headers: {
      secret
    },
    url: s.url + '/response-interceptor'
  })
  t.is(response.body.value, secret)
})
