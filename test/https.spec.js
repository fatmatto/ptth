
const test = require('ava')
const http = require('../index')
const https = require('https')
const { createSSLServer } = require('./helpers/server')

let s

test.before('setup', async () => {
  s = await createSSLServer()

  s.on('/', (request, response) => response.end('ok'))

  await s.listen(s.port)
})

test.after('cleanup', async () => {
  await s.close()
})

test('make request to https server without ca', async t => {
  const response = await http({
    method: 'GET',
    url: s.url,
    agent: new https.Agent({
      rejectUnauthorized: false
    })
  })
  t.truthy(response.body)
})
