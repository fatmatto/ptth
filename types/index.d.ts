export = ptth;
/**
 * Performs an HTTP(s) request
 * @param {Object} config
 * @param {String} config.method Request method
 * @param {String} [config.url] Request uri
 * @param {String} [config.uri] Request uri
 * @param {Object} [config.headers] Request Headers object
 * @param {Object} [config.query] Request query object
 * @param {Object} [config.body] Request body object
 * @param {Object} [config.pipeTo] A writeable stream to attach to the request's readable stream
 * @param {Boolean} [config.json] If set to false the body won't be parsed as json (with JSON.parse). Defaults to true
 *
 * @returns {Promise}
 * @throws {Error}
 */
declare function ptth(config: {
    method: string;
    url?: string;
    uri?: string;
    headers?: any;
    query?: any;
    body?: any;
    pipeTo?: any;
    json?: boolean;
}): Promise<any>;
